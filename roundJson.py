import json


class PlayerInfo:
    def __init__(self, json_obj):
        self.key = json_obj["Key"]
        self.name = json_obj["Name"]
        self.bag = int(json_obj["BombBag"])
        self.radius = int(json_obj["BombRadius"])
        self.points = int(json_obj["Points"])
        self.dead = bool(json_obj["Killed"])
        self.available_bombs = self.bag


class RoundJson:
    def __init__(self, file_path):
        with open(file_path, 'r') as f:
            data = json.load(f)

        self.seed = data["MapSeed"]
        self.width = int(data["MapWidth"])
        self.height = int(data["MapHeight"])
        self.blocks = data["GameBlocks"]
        player_list = [PlayerInfo(player) for player in data["RegisteredPlayerEntities"]]
        self.player_info = {}
        for player in player_list:
            self.player_info[player.key] = player

        # count_player_bombs
        for x in range(self.width):
            for y in range(self.height):
                bomb = self.blocks[x][y]["Bomb"]
                if bomb:
                    owner_key = bomb["Owner"]["Key"]
                    self.player_info[owner_key].available_bombs -= 1

    def get_object_keys_at(self, x, y):
        keys = [' ']
        block = self.blocks[x][y]
        if block["Exploding"]:
            keys.append('*')
        else:
            entity = block["Entity"]
            bomb = block["Bomb"]
            if entity and bomb:
                keys.append(entity["Key"])
                keys.append('@')
                return keys
            elif entity:
                entity_type = str(entity["$type"])
                if "Wall" in entity_type:
                    if "Indestructible" in entity_type:
                        keys.append('#')
                    else:
                        keys.append('+')
                elif "Player" in entity_type:
                    keys.append(entity["Key"])
            if block["PowerUp"]:
                power_up_type = str(block["PowerUp"]["$type"])
                if "Super" in power_up_type:
                    keys.append('$')
                elif "BombRaduis" in power_up_type:
                    keys.append('!')
                elif "BombBag" in power_up_type:
                    keys.append('&')
            if bomb:
                keys.append(bomb["Owner"]["Key"] + 'B')
                keys.append(str(bomb["BombTimer"]))
        return keys
